package com.springmvcProject.Dao;

import java.sql.ResultSet;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import com.springmvcProject.model.Empl;

public class EmplDao {

	private JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public int save(Empl e) {
		String query = "INSERT into Empl (id, name , salary, designation) values(?,?,?,?)";
		return this.jdbcTemplate.update(query, e.getId(), e.getName(), e.getSalary(), e.getDesignation());
	}

	public int update(Empl e) {
		String query1 = "update Empl set name='"+e.getName()+"',salary='"+e.getSalary()+"',designation='"+e.getDesignation()+"'  where id='"+e.getId()+"' ";  
		return this.jdbcTemplate.update(query1);
		
	}

	public int delete(int id) {
		String query = "DELETE FROM Empl WHERE id=?";
		return this.jdbcTemplate.update(query, id);

	}

	public Empl getEmpl(int id) {
		String query = "SELECT * FROM Empl where id=?";
		return this.jdbcTemplate.queryForObject(query,(ResultSet rs, int rowNum) -> new Empl(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getString(4)),id);
	}

	public List<Empl> getEmployees() {
		String query = "SELECT * FROM Empl";
		return jdbcTemplate.query(query, (ResultSet rs, int rowNum) -> new Empl(rs.getInt(1), rs.getString(2),rs.getDouble(3), rs.getString(4)));
	}

}
