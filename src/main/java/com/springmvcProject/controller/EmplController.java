package com.springmvcProject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.springmvcProject.Dao.EmplDao;
import com.springmvcProject.model.Empl;

@Controller
public class EmplController {

	@Autowired
	EmplDao emplDao;

	@RequestMapping("/welcome")
	public String showform(Model m) {
		m.addAttribute("empl", new Empl());
		
		return "emplform";

	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@Valid @ModelAttribute("empl") Empl empl,  BindingResult br) {
		if(br.hasErrors()) {
			return "emplform";
		}
		else {
			emplDao.save(empl);
			return "redirect:/viewform";

		}
		
	}

	@RequestMapping(value = "/editempl/{id}")
	public String edit(@PathVariable int id, Model m) {
		Empl empl = emplDao.getEmpl(id);
		m.addAttribute("command", empl);
		return "empledit";
	}

	@RequestMapping(value = "/viewform")
	public String viewmap(Model m) {

		List<Empl> list = emplDao.getEmployees();
		m.addAttribute("list", list);
		return "viewform";
	}

	@RequestMapping(value = "/editsave", method = RequestMethod.POST)
	public String editsave( @ModelAttribute("empl") Empl empl) {
		
		emplDao.update(empl);
		return "redirect:/viewform";
		

	}

	@RequestMapping(value = "/deleteempl/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable int id) {
		emplDao.delete(id);
		return "redirect:/viewform";
	}
}
